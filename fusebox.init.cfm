<cfsilent>

    <!--- Global XFA --->
    <!--- <cfset xfa.employees_main = "employees.main" />
    <cfset xfa.show_save_employee_form = "employees.save_form" />
    <cfset xfa.do_save_employee = "employees.save" />
    <cfset xfa.do_delete_employee = "employees.delete" />
    <cfset xfa.do_view_employee = "employees.view" />

    <cfset xfa.departments_main = "departments.main" /> --->
    <!--- Initialize applicaiton manager components. --->
    <cfparam name="url.reload" type="string" default="false"/>
    <!--- <cfif url.reload> --->
        <cfset application.traineesmanager = createObject('component', 'tyrofusebox.components.Trainees.TraineesManager').init()/>
        <cfset application.cmn_logger = createObject('component', 'tyrofusebox.components.Lib.Common.cmn_logger').init()/>
        <cfobject type="component" component="tyrofusebox.components.Lib.Common.cmn_logger" name="application.cmn_logger_class"> 
        <!--- <cfdump var = #application.traineesmanager# /> --->
        <!---<cfset application.employeesmanager = createObject('component', 'tyrofusebox.components.Employees.EmployeesManager').init()/>
        <cfset application.departmentsmanager = createObject('component', 'coldfusion-fusebox5-test.components.Departments.DepartmentsManager').init()/>
        <cfset application.dataformats = createObject('component', 'coldfusion-fusebox5-test.components.Util.DataFormats').init()/>          --->   
    <!--- </cfif> --->

    <!--- Set app constants. --->
    <cfset self = "index.cfm">
    <cfset mySelf = "#urlSessionFormat('#self#')#"/>

    <cfif findNoCase('index.cfm;', mySelf)>
        <cfset mySelf = replace(mySelf, 'index.cfm;', 'index.cfm?')/>
    </cfif>

    <cfif right(mySelf, 9) EQ "index.cfm">
        <cfset mySelf = mySelf & "?"/>
    <cfelse>
        <cfset mySelf = mySelf & "&"/>
    </cfif>

    <cfset mySelf = mySelf & "#application.fusebox.fuseactionVariable#="/>

</cfsilent>
