<circuit access="public">
    <fuseaction name="test">
        <do action="v_trainees.test"/>
    </fuseaction>

    <fuseaction name="main">
        <do action="v_trainees.main"/>
    </fuseaction>

    <fuseaction name="trainees_get">
    	<do action="m_trainees.get_sel"/>
        <do action="v_trainees.dsp"/>
    </fuseaction>

    <fuseaction name="trainees_add">
    	<do action="m_trainees.trainee_add"/>
		<xfa name="type" value="add" />
        <do action="v_trainees.dsp"/>
    </fuseaction>

    <fuseaction name="trainees_edit">
    	<do action="m_trainees.trainee_edit"/>
		<xfa name="type" value="edit" />
        <do action="v_trainees.dsp"/>
    </fuseaction>

    <fuseaction name="trainees_del">
    	<do action="m_trainees.trainee_del"/>
		<xfa name="type" value="del" />
        <do action="v_trainees.dsp"/>
    </fuseaction>
</circuit>
