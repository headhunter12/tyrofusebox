<circuit access="public">

    <prefuseaction>
        <do action="m_employees.get_employees_desc" />
        <do action="m_departments.get_departments" />
    </prefuseaction>

	<fuseaction name="main">
        <do action="v_employees.header" contentvariable="header_content" />
        <do action="v_departments.body" contentvariable="body_content" />
        <do action="v_employees.footer" contentvariable="footer_content" />
	</fuseaction>

</circuit>
