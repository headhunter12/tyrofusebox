<circuit access="public">

    <prefuseaction>
        <do action="m_employees.get_employees_desc" />
        <do action="m_departments.get_departments" />
    </prefuseaction>

    <postfuseaction>
    </postfuseaction>

    <fuseaction name="main">
        <do action="v_employees.header" contentvariable="header_content" />
        <do action="v_employees.body" contentvariable="body_content" />
        <do action="v_employees.footer" contentvariable="footer_content" />
    </fuseaction>

    <fuseaction name="save_form">
        <if condition="isDefined('url.id')">
            <true>
                <do action="m_employees.get_employee_by_id" />
            </true>
        </if>

        <do action="v_employees.header" contentvariable="header_content" />
        <do action="v_employees.save_form" contentvariable="body_content" />
        <do action="v_employees.footer" contentvariable="footer_content" />
    </fuseaction>

    <fuseaction name="save">
        <do action="m_employees.save_employee"/>
    </fuseaction>

    <fuseaction name="view">
        <if condition="isDefined('url.id')">
            <true>
                <do action="m_employees.get_employee_by_id" />
                <do action="m_departments.get_departments" />
                <do action="v_employees.header" contentvariable="header_content" />
                <do action="v_employees.view_lp" contentvariable="body_content" />
                <do action="v_employees.footer" contentvariable="footer_content" />
            </true>
            <false>
                <relocate url="http://127.0.0.1:8500/coldfusion-fusebox5-test/" />
            </false>
        </if>
    </fuseaction>

    <fuseaction name="delete">
        <do action="m_employees.delete_employee" />
    </fuseaction>

    <fuseaction name="reload_summary_view">
        <do action="m_employees.get_employees_desc" />
        <do action="m_departments.get_departments" />
        <set name="page_content" value = "#application.dataformats.QueryToArrayOfStructures(get_employees_list_desc)#" />
    </fuseaction>

    <fuseaction name="search_keyword_category">
        <do action="m_employees.get_employee_by_keyword_category" />
        <do action="m_departments.get_departments" />
        <set name="page_content" value = "#application.dataformats.QueryToArrayOfStructures(get_employee_by_keyword_category)#" />
    </fuseaction>

    <fuseaction name="upload_file">
        <do action="m_employees.upload_file" />
    </fuseaction>

    <fuseaction name="set_employee_image">
        <do action="m_employees.set_employee_image" />
    </fuseaction>
</circuit>
