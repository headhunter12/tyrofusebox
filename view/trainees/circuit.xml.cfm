<circuit access="internal">
	<fuseaction name="test">
    	<include template="test" />
    </fuseaction>

    <fuseaction name="main">
    	<include template="header" />
        <include template="body" />
        <include template="footer" />
    </fuseaction>

    <fuseaction name="dsp">
        <include template="ajax.cfm" />
    </fuseaction>
</circuit>

