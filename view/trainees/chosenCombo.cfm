<cfquery name="country_all_sel" datasource="tyr_datasource">
	SELECT * FROM country
</cfquery>
<div class="form-group">
	<label for="select1" style="margin-top:5px; float:left; padding: 0px 3px; width: 100px">Country: </label>
	<div style="width:173px; float:left;">
		<select class="form-control" id="select1" data-placeholder="Choose a Country..." tabindex="-9999">
			<option value=""></option>
			<cfloop query="country_all_sel">
				<cfoutput><option value="#country_all_sel.id#">#country_all_sel.country_name#</option></cfoutput>
			</cfloop>
			<!--- <option value=""></option>
			<option value="United States">United States</option>
			<option value="United Kingdom">United Kingdom</option> --->
		</select>
	</div>
</div>