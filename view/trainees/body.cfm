<body style="padding-top:50px">
<cfscript> 
	function valueAndTypeCompare(v1, v2) {
	    return v1 EQ v2 AND (getMetadata(v1).getName() EQ getMetadata(v2).getName());  
	}
</cfscript>

<div class="dataTables_wrapper">
  <table class="dataTable" id="trainee_table">
  	<thead>
  	    <tr>
  	        <th>ID</th>
  	        <th>First Name</th>
  	        <th>Last Name</th>
  	        <th>Country</th>
  	    </tr>
  	</thead>
  </table>
</div>

<!-- Modal ADD-->
<div class="modal fade" id="modalAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">Add Trainee</h4>
      </div>
      <div class="modal-body">
        <form id="formAdd" action="cfAddTrainee.cfm" class="form-horizontal" role="form">
          <fieldset>
          	<div class="form-group">
  	        	<label for="firstname" style="margin-top:5px; float:left; padding: 0px 3px; width: 100px">Firstname: </label>
  	        	<input type="text" class="addTexts" id="firstname" name="firstname"/>
  	        </div>
  	        <div class="form-group">
  	        	<label for="lastname" style="margin-top:5px; float:left; padding: 0px 3px; width: 100px">Lastname: </label>
          		<input type="text" class="addTexts" id="lastname" name="lastname" style="margin-top:5px"/>
          	</div>

          	<cfinclude template="chosenCombo.cfm" />
          </fieldset>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="saveAdd" data-actionmode="save" data-editid="0">Save</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal ERROR MSG-->
<div class="modal fade" id="modalMsg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <!--- <h4 class="modal-title alert alert-danger" id="myModalLabel">Error</h4> --->
      </div>
      <div class="modal-body alert-danger">
        <strong id="modalErrorMsg">Please select an item first.</strong>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal" id="closeBtn">Close</button>
        <button style="display:none" type="button" class="btn btn-primary" id="confirmDelete">Delete</button>
      </div>
    </div>
  </div>
</div>

</body>