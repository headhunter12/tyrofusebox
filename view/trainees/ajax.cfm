<cfset request_type = !isDefined("variables.xfa") ? "get" : (!isDefined("variables.xfa.type") ? "get" : #variables.xfa.type#) />

<cfif #request_type# EQ "get">
	<cfheader name="Content-Type" value="application/json" />
	<cfoutput>#SerializeJSON(m_dsp)#</cfoutput>	
<cfelseif #request_type# EQ "trainees.add">
	<cfoutput>#m_dsp#</cfoutput>
<cfelseif #request_type# EQ "trainees.edit">
	<cfoutput>#m_dsp#</cfoutput>
<cfelseif #request_type# EQ "trainees.del">
	<cfoutput>#m_dsp#</cfoutput>
</cfif>

