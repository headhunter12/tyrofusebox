<html>
	<head>
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.2.1/css/buttons.dataTables.min.css">
	<link href="<cfoutput>#request.css_path#</cfoutput>bootstrap/css/bootstrap.css" rel="stylesheet">
	<link href="<cfoutput>#request.css_path#</cfoutput>chosen-bootstrap.css" rel="stylesheet">
	<link href="<cfoutput>#request.css_path#</cfoutput>styles.css" rel="stylesheet">
	<script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=" crossorigin="anonymous">
	</script>
	<script src="<cfoutput>#request.js_path#</cfoutput>bootstrap/bootstrap.js">
	</script>
	<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js">
	</script>
	<script src="https://cdn.datatables.net/buttons/1.2.1/js/dataTables.buttons.min.js">
	</script>
	<script src="<cfoutput>#request.js_path#</cfoutput>chosen/chosen.jquery.js">
	</script>	

	<script type="text/javascript">
		GLOBAL_OBJ = (function() {
			var data = true;

			function set (data_) {
				return data = data_;
			}

			return {
				hasNotRunOnce : function() {
					return data;
				},
				set : function (data_) {
					return set(data_);
				}
			}
		})();

		var g_table = null;
		$(function() {
			$('#trainee_table').on( 'draw.dt', function () {
			    GLOBAL_OBJ.set(false);
			    
			} );

			g_table = $('#trainee_table').DataTable({
       			dom: 'Bfrtip',
				"ajax": "index.cfm?action=trainees.trainees_get&reload=" + GLOBAL_OBJ.hasNotRunOnce(),
				createdRow: function( row, data, dataIndex ) {
			        // Set the data-status attribute, and add a class

			        $( row ).attr('data-countryid', data.country_id);
			    },
			    "buttons": [
			        {
			        	id : "add",
			        	className: "btn",
		                text: 'ADD',
		                action: function ( e, dt, node, config ) {
		                	$("#saveAdd").attr('actionmode', "add");
		                	$('#modalAdd').find('#firstname').val("");
		                	$('#modalAdd').find('#lastname').val("");
		                	$('#modalAdd').modal('show'); 
		                    /*$('#myModal').on('shown.bs.modal', function () {
							  $('#myInput').focus()
							});*/
		                }
		            },
			        {
			        	id: "edit",
			        	className: "btn",
		                text: 'EDIT',
		                action: function ( e, dt, node, config ) {
			            	$errMsg = $('#modalMsg').find('#modalErrorMsg');
		                	try {
		                		$row = $(".selected").find('td');
			                	id = $row[0].innerHTML;
			                	firstname = $row[1].innerHTML;
			                	lastname = $row[2].innerHTML;
			                	country_id = $(".selected").attr('data-countryid');


		                		$("#saveAdd").attr('actionmode', "edit");

			                	$('#modalAdd').find('#firstname').val(firstname);
			                	$('#modalAdd').find('#lastname').val(lastname);

				                $('#modalAdd').modal('show');
		                		$("#saveAdd").attr('editid', id);
		                		$('#select1').val(country_id).trigger('chosen:updated');
		                	} catch (e) {
			                	$errMsg.parent().removeClass('alert-success');
			                	$errMsg.parent().addClass('alert-danger');
			                	$errMsg.html("Please select an item first.");
			                	$('#modalMsg').modal('show');	
		                	}
		                }
		            },
			        {
			        	id: "delete",
			        	className: "btn",
		                text: 'DELETE',
		                action: function ( e, dt, node, config ) {

			            	$errMsg = $('#modalMsg').find('#modalErrorMsg');
			                try {
			                	id = $(".selected").find('td')[0].innerHTML;
			                	
			                	if (!confirmMsg('delete'))
			                		return;
			                } catch (e) {
			                	$errMsg.parent().removeClass('alert-success');
			                	$errMsg.parent().addClass('alert-danger');
			                	$errMsg.html("Please select an item first.");
			                	$('#modalMsg').modal('show');	
			                }
		                }
		            },
			    ],
		        "columns": [
		            { "data": "id" },
		            { "data": "firstname" },
		            { "data": "lastname" },
		            { "data": "country_name" }
		        ]
			});

			$('#trainee_table tbody').on( 'click', 'tr', function () {
		        if ( $(this).hasClass('selected') ) {
		            $(this).removeClass('selected');
		        }
		        else {
		            $('tr.selected').removeClass('selected');
		            $(this).addClass('selected');
		        }
		    } );
		 
		    $('#button').click( function () {
		        table.row('.selected').remove().draw( false );
		    } );

		    $('#saveAdd').click( function () {
            	$errMsg = $('#modalMsg').find('#modalErrorMsg');

		    	var modal_mode = $("#saveAdd").attr("actionmode");
		    	var actionmode = modal_mode == "add" ? "add" : "edit";
		    	var params = {request_type: actionmode};
		    	params.firstname = $("#firstname").val();
		    	params.lastname = $("#lastname").val();
		    	params.country = $("#select1").val();
		    	params.editid = $(this).attr("editid");

		    	if (params.firstname.length == 0 || params.lastname.length == 0 ||  $("#select1").val().length == 0)
		    		return;

		    	ajaxCall(actionmode, params, function (result) {
		    		$errMsg.parent().removeClass('alert-danger');
                	$errMsg.parent().addClass('alert-success');
            		$errMsg.html(result);
					g_table.ajax.reload();

        			$('#confirmDelete').hide();
            		$('#modalMsg').modal('show');
		    	});

		        $('#modalAdd').modal('hide');
		    });

		    $('#lastname').keyup(function(e){
			    if(e.keyCode == 13)
			    {
			        $('#saveAdd').trigger("click");
			    }
			});

			$('#confirmDelete').click( function () {
            	$errMsg = $('#modalMsg').find('#modalErrorMsg');
            	id = $(".selected").find('td')[0].innerHTML;
            	var params = { request_type : "delete", id : id};

            	ajaxCall('del', params, function(data) {
                	$errMsg.parent().removeClass('alert-danger');
                	$errMsg.parent().addClass('alert-success');
            		$errMsg.html(data);
					g_table.ajax.reload();

        			$('#confirmDelete').hide();
            		$('#modalMsg').modal('show');
            	});
		    });

		    $('#modalMsg').on('hidden.bs.modal', function () {
        		$('#confirmDelete').hide();
			})

			$('#trainee_table').on('dblclick', 'td', function(e) {

				$errMsg = $('#modalMsg').find('#modalErrorMsg');
            	try {
            		// $row = $(".selected").find('td');
            		$row = $($(this).parent()[0]).find('td');

                	id = $row[0].innerHTML;
                	firstname = $row[1].innerHTML;
                	lastname = $row[2].innerHTML;
                	country_id = getCountryByID(id);

            		$("#saveAdd").attr('actionmode', "edit");

                	$('#modalAdd').find('#firstname').val(firstname);
                	$('#modalAdd').find('#lastname').val(lastname);
            		$('#select1').val(country_id).trigger('chosen:updated');

	                $('#modalAdd').modal('show');
            		$("#saveAdd").attr('editid', id);
            	} catch (e) {
                	$errMsg.parent().removeClass('alert-success');
                	$errMsg.parent().addClass('alert-danger');
                	$errMsg.html("Please select an item first.");
                	$('#modalMsg').modal('show');	
            	}
			})

			// ajaxCall({formdatatyro1: "3333"});
			$("#select1").chosen();


			$('.addTexts').on('click', function (e) {
				e.stopPropagation();
			})
		});

		function ajaxCall(type, params, cb) {
			$.ajax({
				url: "index.cfm?action=trainees.trainees_" + type,
				type: "POST",
				data: params,
				success: function(data) {
					cb(data);
				}
			})
		}

		function confirmMsg(type, cb) {
			$errMsg = $('#modalMsg').find('#modalErrorMsg');

			if (type === 'delete') {
				msg = "Are you sure you want to delete the item?";
			} else {
				msg = "";
			}

        	$errMsg.parent().removeClass('alert-success');
        	$errMsg.parent().removeClass('alert-danger');
        	$errMsg.html(msg);
        	$('#confirmDelete').show();
        	$('#modalMsg').modal('show');
		}

		function getCountryByID(trainee_id) {
			// $items = $('#trainee_table').find('tr td:first-child');
			var country_id = null;
			$tr_with_countryid = $('#trainee_table').find('tr[data-countryid]');
			$($tr_with_countryid).each(function(index, item) {
				if (trainee_id == $(item).children().first().text()) {
					country_id = country_id == null ? $(this).data('countryid') : countryid;
					return;
				}
			});

			return country_id;
		}
	</script>
	</head>