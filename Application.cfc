<cfcomponent extends="fusebox5.Application" output="false">
    <cfscript>
        this.name                   = "tyrofusebox";
        this.application_timeout    = createTimeSpan(0, 6, 0, 0);
        this.session_management     = true;
        this.session_timeout        = createTimeSpan(0, 0, 30, 0);
        this.client_management      = true;
        this.dsn                    = "testcf";
        this.webmaster_email        = "webmaster@localhost";

        function onRequestStart (targetPage) {
            super.onRequestStart(arguments.targetPage);
            //code formerly in fusebox.init.cfm
            self = myFusebox.getSelf();
            myself = myFusebox.getMyself();
            if (listFirst(CGI.SERVER_NAME, ".") == "www") {
                FUSEBOX_PARAMETERS.mode = "production";
            } else {
                FUSEBOX_PARAMETERS.mode = "development-full-load";
            }
            // request scope
            request.application_name    = this.name;
            request.dsn                 = this.dsn;
            request.webmaster_email     = this.webmaster_email;
            request.base_dir            = GetDirectoryFromPath(GetBaseTemplatePath());
            request.component_path      = "components.";
            request.image_path          = "assets/images/";
            request.css_path            = "assets/css/";
            request.js_path             = "assets/js/";
            request.flash_path          = "assets/multimedia/";
            request.video_path          = "assets/multimedia/";
            request.pps_path            = "assets/multimedia/";
        }
    </cfscript>

</cfcomponent>

