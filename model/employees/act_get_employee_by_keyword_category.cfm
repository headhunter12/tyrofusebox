<cfswitch expression="#url.category#">
	<cfcase value="id">
		<cfset id = 0 />
		<cfif #keyword# NEQ "" AND isNumeric(url.keyword)>
			<cfset id = #keyword# />
		</cfif>
	    <cfset get_employee_by_keyword_category = "#application.employeesmanager.GetEmployeeById(id)#" />
	</cfcase>  

	<cfcase value="department_id">
		<cfset department_id = 0 />
		<cfset get_department_by_name = "#application.departmentsmanager.GetDepartmentByName(url.keyword)#" />
		
		<cfif get_department_by_name.recordCount NEQ 0>
			<cfset department_id = #get_department_by_name['id'][1]# />
		</cfif>

		<cfset get_employee_by_keyword_category = "#application.employeesmanager.GetEmployeeByDepartmentIdDesc(department_id)#" />
	</cfcase>

	<cfcase value="full_name">
		<cfset get_employee_by_keyword_category = "#application.employeesmanager.GetEmployeeByFullNameIdDesc(url.keyword)#" />
	</cfcase>

	<cfdefaultcase> 
		<cfset get_employee_by_keyword_category = "#application.employeesmanager.GetEmployeeListDesc()#" />
	</cfdefaultcase> 
</cfswitch>