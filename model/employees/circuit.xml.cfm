<circuit access="internal">

    <fuseaction name="get_employees_desc">
        <include template="act_get_employees_desc.cfm" />
    </fuseaction>

    <fuseaction name="get_employee_by_id">
        <include template="act_get_employee_by_id.cfm" />
    </fuseaction>

    <fuseaction name="get_employee_by_keyword_category">
        <include template="act_get_employee_by_keyword_category.cfm" />
    </fuseaction>

    <fuseaction name="save_employee">
        <include template="act_save_employee.cfm" />
    </fuseaction>

    <fuseaction name="delete_employee">
        <include template="act_delete_employee.cfm" />
    </fuseaction>

    <fuseaction name="upload_file">
        <include template="act_upload_file.cfm" />
    </fuseaction>

    <fuseaction name="set_employee_image">
        <include template="act_set_employee_image.cfm" />
    </fuseaction>
</circuit>
