<cfset set_employee_image = "#application.employeesmanager.SetEmployeeImage(form.id, form.filename)#" />

<cfset response = structNew() />
<cfset response['success'] = false />

<cfif structKeyExists(set_employee_image, 'recordCount') AND set_employee_image.recordCount EQ 1>
    <cfset response['success'] = true />
</cfif>

<cfset page_content = response />