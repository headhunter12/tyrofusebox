<circuit access="internal">

    <fuseaction name="get_sel">
        <include template="act_get_trainees.cfm" />
    </fuseaction>

    <fuseaction name="trainee_add">
        <include template="act_add_trainee.cfm" />
    </fuseaction>

    <fuseaction name="trainee_edit">
        <include template="act_edit_trainee.cfm" />
    </fuseaction>

    <fuseaction name="trainee_del">
        <include template="act_del_trainee.cfm" />
    </fuseaction>

</circuit>
