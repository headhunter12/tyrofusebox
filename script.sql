CREATE TABLE trainee
(
id int NOT NULL identity,
firstname varchar (255),
lastname varchar (255)
)

DELETE FROM trainee
SELECT * FROM trainee

CREATE TABLE country
(
id int NOT NULL PRIMARY KEY IDENTITY,
country_name NVARCHAR (100),
tld NVARCHAR (3)
)

CREATE TABLE country_assignment
(
id int NOT NULL PRIMARY KEY IDENTITY,
country_id int,
trainee_id int
)

INSERT INTO country_assignment (country_id, trainee_id) VALUES (1,31);
SELECT * FROM country WHERE country_name LIKE '%phili%'
SELECT * FROM country_assignment

CREATE PROCEDURE all_trainee_sel
AS 
SELECT a.*, c.country_name, c.id AS country_id FROM trainee a
		LEFT JOIN country_assignment b ON b.trainee_id = a.id
		LEFT JOIN country c ON c.id = b.country_id



CREATE PROCEDURE trainee_info_insert @firstname nvarchar(50), @lastname nvarchar(50), @country_id int
AS
INSERT INTO trainee (firstname, lastname) VALUES (@firstname, @lastname);
INSERT INTO country_assignment (country_id, trainee_id) VALUES (@country_id, IDENT_CURRENT('trainee'));
GO

CREATE PROCEDURE trainee_info_update @id int, @firstname nvarchar(50), @lastname nvarchar(50), @country_id int
AS
UPDATE trainee SET firstname = @firstname, lastname = @lastname WHERE id = @id;
UPDATE country_assignment SET country_id = @country_id WHERE trainee_id = @id;
GO

SELECT definition
FROM sys.sql_modules 
WHERE object_id = OBJECT_ID('trainee_info_update')

CREATE PROCEDURE trainee_info_del @id int
AS
BEGIN TRANSACTION
DELETE FROM trainee WHERE id = @id;
DELETE FROM country_assignment WHERE trainee_id = @id;
COMMIT TRANSACTION
