<cfsetting enablecfoutputonly="true" />
<cfprocessingdirective pageencoding="utf-8" />
<!--- circuit: trainees --->
<!--- fuseaction: trainees_add --->
<cftry>
<cfset myFusebox.thisPhase = "appinit">
<cfset myFusebox.thisCircuit = "trainees">
<cfset myFusebox.thisFuseaction = "trainees_add">
<cfif myFusebox.applicationStart or
		not myFusebox.getApplication().applicationStarted>
	<cflock name="#application.ApplicationName#_fusebox_#FUSEBOX_APPLICATION_KEY#_appinit" type="exclusive" timeout="30">
		<cfif not myFusebox.getApplication().applicationStarted>
			<cfset myFusebox.getApplication().applicationStarted = true />
		</cfif>
	</cflock>
</cfif>
<!--- do action="m_trainees.trainee_add" --->
<cfset myFusebox.thisPhase = "requestedFuseaction">
<cfset myFusebox.thisCircuit = "m_trainees">
<cfset myFusebox.thisFuseaction = "trainee_add">
<cftry>
<cfoutput><cfinclude template="../model/trainees/act_add_trainee.cfm"></cfoutput>
<cfcatch type="missingInclude"><cfif len(cfcatch.MissingFileName) gte 19 and right(cfcatch.MissingFileName,19) is "act_add_trainee.cfm">
<cfthrow type="fusebox.missingFuse" message="missing Fuse" detail="You tried to include a fuse act_add_trainee.cfm in circuit m_trainees which does not exist (from fuseaction m_trainees.trainee_add).">
<cfelse><cfrethrow></cfif></cfcatch></cftry>
<cfset myFusebox.thisCircuit = "trainees">
<cfset myFusebox.thisFuseaction = "trainees_add">
<cfset xfa.type = "trainees.add" />
<!--- do action="v_trainees.dsp" --->
<cfset myFusebox.thisCircuit = "v_trainees">
<cfset myFusebox.thisFuseaction = "dsp">
<cftry>
<cfoutput><cfinclude template="../view/trainees/ajax.cfm"></cfoutput>
<cfcatch type="missingInclude"><cfif len(cfcatch.MissingFileName) gte 8 and right(cfcatch.MissingFileName,8) is "ajax.cfm">
<cfthrow type="fusebox.missingFuse" message="missing Fuse" detail="You tried to include a fuse ajax.cfm in circuit v_trainees which does not exist (from fuseaction v_trainees.dsp).">
<cfelse><cfrethrow></cfif></cfcatch></cftry>
<cfcatch><cfrethrow></cfcatch>
</cftry>

