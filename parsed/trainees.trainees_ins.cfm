<cfsetting enablecfoutputonly="true" />
<cfprocessingdirective pageencoding="utf-8" />
<!--- circuit: trainees --->
<!--- fuseaction: trainees_ins --->
<cftry>
<cfset myFusebox.thisPhase = "appinit">
<cfset myFusebox.thisCircuit = "trainees">
<cfset myFusebox.thisFuseaction = "trainees_ins">
<cfif myFusebox.applicationStart or
		not myFusebox.getApplication().applicationStarted>
	<cflock name="#application.ApplicationName#_fusebox_#FUSEBOX_APPLICATION_KEY#_appinit" type="exclusive" timeout="30">
		<cfif not myFusebox.getApplication().applicationStarted>
			<cfset myFusebox.getApplication().applicationStarted = true />
		</cfif>
	</cflock>
</cfif>
<cfset myFusebox.thisPhase = "requestedFuseaction">
<cfset xfa.type = "trainees.add" />
<!--- do action="v_trainees.dsp" --->
<cfset myFusebox.thisCircuit = "v_trainees">
<cfset myFusebox.thisFuseaction = "dsp">
<cftry>
<cfoutput><cfinclude template="../view/trainees/ajax.cfm"></cfoutput>
<cfcatch type="missingInclude"><cfif len(cfcatch.MissingFileName) gte 8 and right(cfcatch.MissingFileName,8) is "ajax.cfm">
<cfthrow type="fusebox.missingFuse" message="missing Fuse" detail="You tried to include a fuse ajax.cfm in circuit v_trainees which does not exist (from fuseaction v_trainees.dsp).">
<cfelse><cfrethrow></cfif></cfcatch></cftry>
<cfcatch><cfrethrow></cfcatch>
</cftry>

