<cfcomponent extends="fusebox5.Application" output="false">

    <cfset this.name = right(REReplace(getDirectoryFromPath(getCurrentTemplatePath()),'[^A-Za-z]','','all'),64) />
	<cfset FUSEBOX_PARAMETERS.debug = false />
	<cfset FUSEBOX_CALLER_PATH = getDirectoryFromPath(getCurrentTemplatePath()) />

</cfcomponent>