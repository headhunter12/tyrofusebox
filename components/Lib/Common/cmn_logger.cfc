<cfcomponent displayname="CommonLogger">
	<cffunction name="Init" access="public" returntype="cmn_logger" output="false" displayname="" hint="">
		<cfargument name="file_name" required="false" default="log" type="string"/>
		<cfset variables.filename = "#arguments.file_name#.log" /> <!--- log.log --->
		<cfset variables.full_filename = arguments.file_name /> <!--- log.log --->
		<cfset variables.full_base_path = "#application.fusebox.APPROOTDIRECTORY#logs/" />
        <cfreturn this />
    </cffunction>

	<cffunction name="writeToFile" returntype="void">
		<cfargument name="absolute_path" required="yes" type="string"/>
		<cfargument name="msg" required="yes" type="string"/>

		<cffile
			action = "append"
			file = "#arguments.absolute_path#"
			output = "#msg#"
			addNewLine = "yes"
		>
	</cffunction>

	<cffunction name="log" returntype="void">
		<cfargument name="msg" required="true" type="string"/>
		<cfargument name="logName" required="false" type="string" default="#variables.filename#"/>

		<cfset ts_obj = getTimestamp() />

		<cfset var local.logName = "#arguments.logName#" />
		<cfset var abs_path = "#variables.full_base_path##ts_obj.date#-#local.logName#" />

		<cfset var timed_msg = "[" & "#ts_obj.time#" & "] - #arguments.msg#" />

		<cffile
			action = "append"
			file = "#local.abs_path#"
			output = "#local.timed_msg#"
			addNewLine = "yes"
		>
	</cffunction>


	<!--- to be abstracted --->
	<cffunction name="setName" returntype="cmn_logger">
		<cfargument name="filename" type="string" required="yes">
		<cfargument name="extn" type="string" required="false" default=".log">
		<cfset variables.filename = "#arguments.filename##arguments.extn#" />

		<cfreturn this />
	</cffunction>

	<cffunction name="getTimestamp" returntype="struct" access="private">
		<!--- <cfargument name="format" type="string" required="false" default="YYYY-mm-dd" />
		<cfargument name="date" type="string" required="false" default="#now()#" /> --->
		<cfset date = "#now()#" />

		<cfreturn {
			<!--- "dateTime" = #LSDateFormat(date, "YYYY-mm-dd hh:mm:ss")#, --->
			"date" = #DateFormat(date, "YYYY-mm-dd")#,
			"time" = #TimeFormat(date, "hh:mm:ss")#
		} />
	</cffunction>

</cfcomponent>