<cfcomponent displayname="TraineesManager">
	<cffunction name="Init" access="public" returntype="TraineesManager" output="false" displayname="" hint="">
        <cfreturn this />
    </cffunction>
	<cffunction name="get_sel">
		<cftry>
			<cfstoredproc procedure="all_trainee_sel" datasource="tyr_datasource">
				<cfprocresult name="select_all_query" />
			</cfstoredproc>

			<cfcatch type="database">
				<cfset variables.logger = #application.cmn_logger_class.init("TraineesManager.get_sel")# />

	            <cfsavecontent variable="errorDetails">
				    <cfdump var="#cfcatch#" expand="yes" format="text">
				</cfsavecontent>

				<cfscript>
					variables.logger.log(#errorDetails#);
				</cfscript>
				<cfreturn "Internal Application Error." />
			</cfcatch>
		</cftry>		

		<cfset jsonData = serializeSelectTrainee(#select_all_query#) />
		<cfreturn #jsonData# />
	</cffunction>

	<cffunction name="serializeSelectTrainee" returntype="any">
		<cfargument name="result_set" type="query" required="yes">
		<!--- <cfset all_data = #SerializeJSON()# />--->
		<cfset result_data = [] >
		<cfloop query="result_set">
			<cfscript> 
				ArrayAppend(result_data, { "id" = #result_set.id#,
					"firstname" = #result_set.firstname#,
					"lastname" = #result_set.lastname#,
					"country_name" = #result_set.country_name#,
					"country_id" = #result_set.country_id#
				});
			</cfscript>
		</cfloop>

		<cfset result = StructNew() >
		<cfset result["data"] = result_data>
		<cfreturn #result# >
	</cffunction>

	<cffunction name="insertTrainee" returntype="string">
		<cfargument name="firstname" type="string" required="yes">
		<cfargument name="lastname" type="string" required="yes">
		<cfargument name="country" type="string" required="yes">

		<cftry>
			<cfstoredproc procedure="trainee_info_insert" datasource="tyr_datasource">
				<cfprocparam type="in" null="no" cfsqltype="CF_SQL_VARCHAR" dbvarname="@firstname" value="#arguments.firstname#"></cfprocparam>
				<cfprocparam null="no" type="in" cfsqltype="CF_SQL_VARCHAR" dbvarname="@lastname" value="#arguments.lastname#"></cfprocparam>
				<cfprocparam null="no" type="in" cfsqltype="CF_SQL_VARCHAR" dbvarname="@country_id" value="#arguments.country#"></cfprocparam>
			</cfstoredproc>

			<cfcatch type="database">
				<cfset variables.logger = #application.cmn_logger_class.init("TraineesManager.insertTrainee")# />

	            <cfsavecontent variable="errorDetails">
				    <cfdump var="#cfcatch#" expand="yes" format="text">
				</cfsavecontent>

				<cfscript>
					variables.logger.log(#errorDetails#);
				</cfscript>
				<cfreturn "Internal Application Error." />
			</cfcatch>
		</cftry>

		<cfreturn "Successfully added new trainee.">
	</cffunction>
	<cffunction name="editTrainee" returntype="string">
		<cfargument name="id" type="string" required="yes">
		<cfargument name="firstname" type="string" required="yes">
		<cfargument name="lastname" type="string" required="yes">
		<cfargument name="country" type="string" required="yes">

		<cftry>
			<cfstoredproc procedure="trainee_info_update" datasource="tyr_datasource">
				<cfprocparam type="in" null="no" cfsqltype="CF_SQL_INTEGER" dbvarname="@id" value="#val(arguments.id)#"></cfprocparam>
				<cfprocparam type="in" null="no" cfsqltype="CF_SQL_VARCHAR" dbvarname="@firstname" value="#arguments.firstname#"></cfprocparam>
				<cfprocparam type="in" null="no" cfsqltype="CF_SQL_VARCHAR" dbvarname="@lastname" value="#arguments.lastname#"></cfprocparam>
				<cfprocparam type="in" null="no" cfsqltype="CF_SQL_INTEGER" dbvarname="@country_id" value="#val(arguments.country)#"></cfprocparam>
			</cfstoredproc>

			<cfcatch type="database">
				<cfset variables.logger = #application.cmn_logger_class.init("TraineesManager.editTrainee")# />

	            <cfsavecontent variable="errorDetails">
				    <cfdump var="#cfcatch#" expand="yes" format="text">
				</cfsavecontent>

				<cfscript>
					variables.logger.log(#errorDetails#);
				</cfscript>
				<cfreturn "Internal Application Error." />
			</cfcatch>
		</cftry>

		<cfreturn "Successfully updated the item.">
	</cffunction>

	<cffunction name="deleteTrainee" returntype="string">
		<cfargument name="id" type="string">

		<cftry>
			<cfstoredproc procedure="trainee_info_del" datasource="tyr_datasource">
				<cfprocparam type="in" null="no" dbvarname="@id" cfsqltype="CF_SQL_INTEGER" value="#id#"></cfprocparam>
			</cfstoredproc>

			<cfcatch type="database">
				<cfset variables.logger = #application.cmn_logger_class.init("TraineesManager.deleteTrainee")# />

	            <cfsavecontent variable="errorDetails">
				    <cfdump var="#cfcatch#" expand="yes" format="text">
				</cfsavecontent>

				<cfscript>
					variables.logger.log(#errorDetails#);
				</cfscript>
				<cfreturn "Internal Application Error." />
			</cfcatch>
		</cftry>

		<cfreturn "Successfully deleted the item.">
	</cffunction>
</cfcomponent>