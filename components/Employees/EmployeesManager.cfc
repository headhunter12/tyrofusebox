<!---
  Created by Mark Dujali on 7/22/2015.
--->
<cfcomponent displayname="EmployeesManager">
   <!--- Place your content here --->

    <cffunction name="Init" access="public" returntype="EmployeesManager" output="false" displayname="" hint="">
        <cfreturn this />
    </cffunction>

    <cffunction name="GetEmployeeListDesc">
        <cfquery name="results" datasource="#request.dsn#">SELECT * FROM employees ORDER BY id DESC</cfquery>
        <cfreturn results />
    </cffunction>

    <cffunction name="GetEmployeeById">
        <cfargument name="id" type="numeric" required="yes">
        <cfquery name="result" datasource="#request.dsn#">SELECT * FROM employees WHERE id=#id#</cfquery>
        <cfreturn result />
    </cffunction>

    <cffunction name="GetEmployeeByDepartmentIdDesc">
        <cfargument name="department_id" type="numeric" required="yes">
        <cfquery name="results" datasource="#request.dsn#">SELECT * FROM employees WHERE department_id=#department_id# ORDER BY id DESC</cfquery>
        <cfreturn results />
    </cffunction>

    <cffunction name="GetEmployeeByFullNameIdDesc">
        <cfargument name="full_name" type="string" required="yes">
        <cfquery name="results" datasource="#request.dsn#">SELECT * FROM employees WHERE full_name LIKE '%#full_name#%' ORDER BY id DESC</cfquery>
        <cfreturn results />
    </cffunction>

    <cffunction name="SaveEmployee">
        <cfargument name="form_data" type="struct" required="yes">
        <cfif NOT StructKeyExists(form_data, 'id')>
            <cfquery name="" datasource="#request.dsn#" result="result">
                INSERT INTO employees VALUES (#form_data['department_id']#, '#form_data['full_name']#', null)
            </cfquery>
        <cfelse>
            <cfquery name="" datasource="#request.dsn#" result="result">
                UPDATE employees SET department_id=#form_data['department_id']#, full_name='#form_data['full_name']#' WHERE id=#form_data['id']#
            </cfquery>
        </cfif>
        <cfreturn result />
    </cffunction>

    <cffunction name="SetEmployeeImage">
        <cfargument name="id" type="numeric" />
        <cfargument name="image" type="string" />
        <cfquery name="" datasource="#request.dsn#" result="result">
            UPDATE employees SET image='#image#' WHERE id='#id#'
        </cfquery>
        <cfreturn result />
    </cffunction>

    <cffunction name="DeleteEmployee">
        <cfargument name="id" type="numeric" required="yes">
        <cfquery name="deleteEmployee" datasource="#request.dsn#" result="result">DELETE FROM employees WHERE id=#id#</cfquery>
        <cfreturn result />
    </cffunction>

</cfcomponent>
