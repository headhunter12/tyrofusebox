<fusebox>

	<circuits>
        <circuit alias="home" path="controller/home/" parent="" />
        <circuit alias="trainees" path="controller/trainees/" parent="" />

        <circuit alias="m_trainees" path="model/trainees/" parent="" />

        <circuit alias="v_trainees" path="view/trainees/" parent="" />
        <circuit alias="v_home" path="view/home/" parent="" />

    </circuits>

	<parameters>
		<parameter name="defaultFuseaction" value="home.login" />
        <parameter name="fuseactionVariable" value="action" />
        <!-- possible values: development-circuit-load, development-full-load or production: -->
        <parameter name="mode" value="development-full-load" />
        <parameter name="conditionalParse" value="true" />
        <!-- change this to something more secure: -->
        <parameter name="password" value="skeleton" />
        <parameter name="strictMode" value="true" />
        <parameter name="debug" value="false" />
        <!-- we use the core file error templates -->
        <parameter name="errortemplatesPath" value="/fusebox5/errortemplates/" />

        <!--
            These are all default values that can be overridden:
        <parameter name="fuseactionVariable" value="fuseaction" />
        <parameter name="precedenceFormOrUrl" value="form" />
        <parameter name="scriptFileDelimiter" value="cfm" />
        <parameter name="maskedFileDelimiters" value="htm,cfm,cfml,php,php4,asp,aspx" />
        <parameter name="characterEncoding" value="utf-8" />
        <parameter name="strictMode" value="false" />
        <parameter name="allowImplicitCircuits" value="false" />
        -->
	</parameters>

	<globalfuseactions>
        <appinit>
        </appinit>

        <preprocess>
        </preprocess>

        <!-- <postprocess>
            <fuseaction action="v_layouts.buildpage"/>
        </postprocess> -->
	</globalfuseactions>

    <plugins>
        <phase name="preProcess">
        </phase>
        <phase name="preFuseaction">
        </phase>
        <phase name="postFuseaction">
        </phase>
        <phase name="fuseactionException">
        </phase>
        <phase name="postProcess">
        </phase>
        <phase name="processError">
        </phase>
    </plugins>

</fusebox>
